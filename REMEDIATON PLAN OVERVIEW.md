# Remediation Plan

This Git project represents a remediation plan to bring a distributed application/developer environment under FEDRAMP ATO evalutaion into alignment with CIS Benchmarks at the First (or Low) Level. The environment consists of many engineering teams utilizing 20-25 AMI's in a CI/CD environment. 

Luckily, this is a common organizational structure for an application vendor and can be secured by ensuring that the L1 Benchmarks, primarily focused on access control to the developer environment and reduction of available attack surface, are configured properly across the ecosystem. 

The stakeholders/audience for this project are the sponsor (a non-technical narrative can be found here: [Sponsor Narrative](https://docs.google.com/document/d/1lAvHhHVD7pfblIav6rveqL5zQuAt6yDATCUJVz0R-yQ/edit?usp=sharing)), various engineers, Security Team, Leadership, and 3PAO. There is no sensitive or controlled information in this project. 

Resources Required:
        
Human:  One project-dedicated security analyst to execute scans, review results, ticketize tasks, prioritize tickets, and partner with domain relevant engineering teams to execute changes. Engineering/execution hours required would vary based on volume of negative scan results. 

Technical: admin/IAM access to the target environment, vulnerability scanner/Amazon Inspector, ticketing/workflow software for planning, Identity Tools, and AWS resources (host/target environemnt and storage/compute)

The tickets generated should be worked into the regular flow of the engineers daily assigned tasks, prioritized in accordance with their existant workload by their leadership.

The first step would be to utilize an existing in-house vulnerability scanner or the native Amazon Inspector for AWS. The Amazon Inspector automatically maps and triggers alerts to those controls not in alignment with the CIS Benchmarks. Based on these results and how Inspector scores flagged controls, the analyst will then ticketize all tasks, identify the engineering domain, and provide tickets with appropriate urgency evaluation to engineering team for individual assignment and execution.  While the sponsor may not require the technical POA&M Template it makes sense to constantly maintain this document for any audit and change management requirements. A sourced example can be found here: [FEDRAMP POA&M](https://docs.google.com/spreadsheets/d/1AwxFnncQILQKe3ZSe4nbogVqge7ddqqwwiGoC6rrBoI/edit?usp=sharing)

A simple workflow for this remediation plan can be found here: [Remediation Workflow](https://whimsical.com/WZh4aKW7jWemiXCP1VzrKQ)

As tickets are created, assigned, worked, and closed, progress can be monitored by following the [Execution Milestone Burndown Chart](https://gitlab.com/kmetivier123/remediation-plan/-/milestones/3)

Once changes have been executed a confirmation scan should be run with results showing positive acceptance criteria being met. This report should be attached to the parent [Evaluation Milestone](https://gitlab.com/kmetivier123/remediation-plan/-/milestones/4).

As the baseline controls come into alignment with security requirements the focus changes to future requirements and persistent scans to ensure constant compliance. This can be achieved by configuring the Amazon Inspector to run automated scans on the environment. This is done utilizing a LAMBDA function to target those instances where the agent is installed, scheduling the scans, identifying any change criteria, and commiting the configuration to the proper environment. 

The workflow for a triggered event can be found here: [Compliance Event Monitoring](https://whimsical.com/J9dzUCEcY4vaVZMZwiJwtH).

The workflow/architecture for the continuous monitoring can be found here: [Continuos Monitoring](https://whimsical.com/GiLXZVe1pAZ72wU6uEYQyG).

This plan will get our team to the L1 Baseline quickly and efficiently. It should be elastic and allow for changes in requirements. These are guidelines to follow in order to ensure maximum efficiency but should be adaptable to situations on the ground and technical and operational roadblocks along the way. 

Any questions can be sent to kyle.metivier@icloud.com or you can reach me at 207-319-2471.





